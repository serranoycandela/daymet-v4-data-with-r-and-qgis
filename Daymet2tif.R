# This code extracts individual tif files from netcdf Daymet data,
# each tif is a day of the year, containing precipitation in mm
# author: Fidel Serrano-Candela
######################################################################

library(ncdf4)
library(raster)
#Working directory
PATH="/home/fidel/GitLab/serranoycandela/daymet-v4-data-with-r-and-qgis/"
setwd(PATH)

# extract all days from 2020
ifelse(!dir.exists("2020"), dir.create("2020"), "Folder exists already")
precip <- brick("daymet_v4_daily_na_prcp_2020.nc", varname="prcp")
r <- stack(precip)
for(i in 1:nlayers(r)){
  filout=paste0("./2020/prec_",gsub("[.]", "-", substr(names(r), 2, 11))[i],".tif")
  writeRaster(r[[i]], filename=filout, format="GTiff",overwrite=TRUE)
}

# extract all days from 2019
ifelse(!dir.exists("2019"), dir.create("2019"), "Folder exists already")
precip <- brick("daymet_v4_daily_na_prcp_2019.nc", varname="prcp")
r <- stack(precip)
for(i in 1:nlayers(r)){
  filout=paste0("./2019/prec_",gsub("[.]", "-", substr(names(r), 2, 11))[i],".tif")
  writeRaster(r[[i]], filename=filout, format="GTiff",overwrite=TRUE)
}