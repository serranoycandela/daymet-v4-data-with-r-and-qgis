# This code calculates the mean precipitation by municipality
# for each day of the year 2020.
# the output is in the form of a new field for each day of the year
# containing the mean of precipitation
# this code requires the existance of a shapefile with municipalities
# in the same folder as the clipped data
# This code runs in QGIS 3.16
# Author: Fidel Serrano-Candela
#####################################################################

import os
from qgis.core import QgsRasterLayer
import processing

carpeta = "/home/fidel/GitLab/serranoycandela/daymet-v4-data-with-r-and-qgis/2019/clipped"
os.chdir(carpeta) 

municipios = QgsVectorLayer("municipios_preci_2019.shp", "municipios", "ogr") 
list_of_files = sorted( filter( lambda x: os.path.isfile(x),
                        os.listdir(carpeta) ) )
contador = 0
for archivo in list_of_files: 
    if archivo.endswith(".tif"):
        contador += 1
        nombre = archivo[:-4] 
        print(nombre)
        layer = QgsRasterLayer(archivo, nombre)
        prefix = "p_"+str(contador)+'_'
        params = {'INPUT_RASTER': layer,
            'RASTER_BAND': 1, 'INPUT_VECTOR': municipios,
            'COLUMN_PREFIX': prefix, 'STATISTICS': [2]
            }
        result = processing.run("native:zonalstatistics", params)