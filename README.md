# Daymet V4 data with R and QGIS



## Getting started

Daymet provides long-term, continuous, gridded estimates of daily weather and climatology variables by interpolating and extrapolating ground-based observations through statistical modeling techniques. The Daymet data products provide driver data for biogeochemical terrestrial modeling and have myriad applications in many Earth science, natural resource, biodiversity, and agricultural research areas. Daymet weather variables include daily minimum and maximum temperature, precipitation, vapor pressure, shortwave radiation, snow water equivalent, and day length produced on a 1 km x 1 km gridded surface over continental North America and Hawaii from 1980 and over Puerto Rico from 1950 through the end of the most recent full calendar year.

Daymet Version 4 is available as of December 15, 2020, through all delivery methods on the Get Data page. 
This repository has R and Python scripts to process data obtained from Daymet Version 4.

For more information on Daymet data visit https://daymet.ornl.gov/

<img src="homepage_map.png" width="400px">


## Step 1: Extract individual files from netcdf data set

The data comes in netcdf format with extencion .nc, example:

daymet_v4_daily_na_prcp_2020.nc

To extract the data as dayly rasters, simply use the R script Daymet2tif.R provided in this repository.
To use it just edit the path accordingly and run it. The result will be a new folder along side the .nc data files,
named with the year of the data "2020" for instance, containing the rasters for each day of the year.

<img src="tifs.png" width="400px">

## Step 2: Clip all rasters to a specific region

To clip all rasters produced in step 1, to a region of interest, simply use the Python script clip_all_rasters.py provided in this repository (it runs under QGIS 3.16).
To use it, all you need is a vector extent layer in the folder where the tifs where generated, "2020" for instance, and edit the path accordingly.
The result will be a new folder named "clipped" with all the dayly rasters clipped to the region of interest

<img src="clip.png" width="400px">


## Step 3: Add precipitation mean for each day of the year to a polygon layer

To calculate the mean precipitation for each day and for each polygon, simply use the Python script precipitacion_municipios.py provided in this repository (it runs under QGIS 3.16).
To use it all you need is a vector layer containing polygons that are in the region of the clipped rasters, municipalities for instance, in the "clipped" folder.

<img src="muni.png" width="400px">

The result will be 365 fields added to the layer of polygons, representing the mean of precipitation for each polygon and for each day of the year.

<img src="april4.png" width="400px">



