# Clip all rasters to the same extent
# this code requieres a shapefile named extent.shp located in the
# folder where the tifs are with the desired extent for clipping 
# all the rasters
# This code runs in QGIS 3.16
# Author: Fidel Serrano-Candela
##################################################################

import os
from qgis.core import QgsRasterLayer
import processing
        
carpeta = "/home/fidel/GitLab/serranoycandela/daymet-v4-data-with-r-and-qgis/2019/"
os.chdir(carpeta)
if not os.path.exists('clipped'):
    os.makedirs('clipped')
extent_layer = QgsVectorLayer("extent.shp", "extent", "ogr") 
for archivo in os.listdir(carpeta): 
    if archivo.endswith(".tif"): 
        print(archivo)
        parameters = {
                'ALPHA_BAND': False,
                'CROP_TO_CUTLINE': True,
                'DATA_TYPE': 0,
                'INPUT': archivo,
                'KEEP_RESOLUTION': False,
                'MASK': extent_layer, 
                'MULTITHREADING': False,
                'OPTIONS': '',
                'OUTPUT': './clipped/'+archivo, 
                'SET_RESOLUTION': False,
                'SOURCE_CRS': None,
                'TARGET_CRS': extent_layer.sourceCrs(), 
                'X_RESOLUTION': None,
                'Y_RESOLUTION': None
            }
        resultado = processing.run("gdal:cliprasterbymasklayer", parameters)

